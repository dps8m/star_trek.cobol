Changed name to star\_trek.cobol;  'star\_trek' to match the program\_id, so that 'star\_trek' is the correct program name (instead of ctrek$star\_trek); '.cobol' to match Multics usage.

Changed line 1531 from:

    note. used to be if char (nx) numeric

to

          * note. used to be if char (nx) numeric

